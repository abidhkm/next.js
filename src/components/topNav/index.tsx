import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Link from 'next/link';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import { routes } from '@/constants';
import { size } from '@/styleUtils';

const NavContainer = styled.div`
  .nav-item {
    margin: 0 6px;
  }

  @media only screen and (max-width: ${size.sm}) {
    display: none;
  }
`;

export default function TopNav() {
  const router = useRouter();

  return (
    <NavContainer>
      <AppBar position="static">
        <Toolbar>
          {routes.map(({ href, label }) => (
            <Link key={href} href={href}>
              <p
                className="nav-item"
                style={{
                  textDecoration: href === router.pathname ? `underline` : ``,
                }}
              >
                {label}
              </p>
            </Link>
          ))}
        </Toolbar>
      </AppBar>
    </NavContainer>
  );
}
