import styled from 'styled-components';

export default styled.hr`
  width: calc(100% + 12px);
  margin: 0 -6px;
  border-width: 0.3px;
  opacity: 0.5;
  border-color: ${({ theme }) => theme.colors.fonts.secondary.main};
`;
