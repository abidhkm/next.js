import { Post } from '@/types';
import styled from 'styled-components';

const Text = styled.p`
  font-size: 0.8rem;
  margin: 12px 0;
  color: ${({ theme }) => theme.colors.fonts.secondary.main};
`;

type CommentsType = {
  comments: Post['comments'];
};

const Comments = ({ comments }: CommentsType) => (
  <Text onClick={() => null}>
    View {comments} comment{comments > 1 ? `s` : ``}
  </Text>
);

export default Comments;
