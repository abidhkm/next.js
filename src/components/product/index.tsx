import React, { useContext } from 'react';
import styled from 'styled-components';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import { Post } from '@/types';
import { breakpoints } from '@/styleUtils';
import { AppContext } from '@/state/productsContext';
import Comments from '../comments';
import Like from '../like';
import Tag from '../tag';

const ProductName = styled.p`
  ${({ theme }) => `
font-size: .9rem;
font-weight: 300;
color: ${theme.colors.fonts.tertiary.main};
margin: ${theme.spacing(1) / 2}px 0;
`}
`;

const ProductPrice = styled.p`
  ${({ theme }) => `
font-size: .8rem;
font-weight: bold;
color: ${theme.colors.fonts.tertiary.main};
margin: ${theme.spacing(1) / 2}px 0;
`}
`;

const ImageContainer = styled.div`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  width: calc(100% + 24px);
  margin-left: -12px;
  height: 300px;
  position: relative;
  background-image: url('${(props) => props.url}');

  @media only screen and ${breakpoints.sm} {
    max-width: 500px;
  }
`;

const Details = styled.div`
  position: absolute;
  bottom: 12px;
  left: 12px;
`;

const Favorite = styled.div`
  position: absolute;
  bottom: 12px;
  right: 12px;
`;

const Caption = styled.div`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.fonts.secondary.main};
`;

type ProductType = {
  details: Post;
};

const Product = ({ details }: ProductType) => {
  const {
    id,
    name,
    price,
    like,
    liked,
    caption,
    tags,
    imageUrl,
    comments,
    favorite,
  } = details;

  const { handleFavoriteClick }: any = useContext(AppContext);

  return (
    <>
      <ImageContainer
        onClick={() => {
          handleFavoriteClick({ value: !favorite, id });
        }}
        url={imageUrl}
      >
        <Details>
          <ProductName>{name}</ProductName>
          <ProductPrice>AED {price}</ProductPrice>
        </Details>

        <Favorite>
          {favorite ? (
            <FavoriteIcon style={{ fill: `#f00` }} />
          ) : (
            <FavoriteBorderIcon style={{ fill: `#fff` }} />
          )}
        </Favorite>
      </ImageContainer>
      <div>
        <Like liked={liked} onClick={() => null} count={like} />

        <Caption>{caption}</Caption>
        {tags.map((tag) => (
          <Tag key={tag} text={tag} />
        ))}
        <Comments comments={comments} />
      </div>
    </>
  );
};

export default Product;
