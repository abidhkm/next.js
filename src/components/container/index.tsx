import styled from 'styled-components';
import { breakpoints } from '../../styleUtils';

export default styled.div`
  padding: 12px;
  min-height: 100vh;

  @media only screen and ${breakpoints.sm} {
    padding: 18px 30px;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 50px;
    grid-row-gap: 10px;
  }

  @media only screen and ${breakpoints.lg} {
    padding: 18px 30px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 50px;
    grid-row-gap: 10px;
  }
`;
