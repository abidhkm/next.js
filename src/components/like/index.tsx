import React from 'react';
import styled from 'styled-components';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';

const Container = styled.div`
  display: flex;
  align-items: center;
  margin: ${({ theme }) => theme.spacing(1) + 2}px 0px;
  p {
    margin: 0px 0px 0px ${({ theme }) => theme.spacing(1)}px;
    color: ${({ liked, theme }) =>
      liked
        ? theme.colors.fonts.primary.main
        : theme.colors.fonts.secondary.main};
    font-size: 0.75rem;
  }
  svg {
    font-size: 1.3rem;
    color: ${({ theme }) => theme.colors.fonts.primary.main};
  }
`;

type LikeType = {
  liked: boolean;
  onClick: (ars: any) => any;
  count: number;
};

const Like = ({ liked, onClick, count }: LikeType) => (
  <Container liked={liked} onClick={() => onClick(!liked)}>
    {liked ? <FavoriteIcon /> : <FavoriteBorderIcon />}
    <p>{count} likes</p>
  </Container>
);

export default Like;
