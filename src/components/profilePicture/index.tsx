import styled from 'styled-components';

const Image = styled.img`
  height: 27px;
  width: 27px;
  object-fit: cover;
  border-radius: 20px;
  border: 1px solid;
  margin-right: 6px;
`;

const ProfilePicture = ({ url }: { url: string }) => (
  <Image alt="profile image" src={url} />
);

export default ProfilePicture;
