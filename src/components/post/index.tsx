import React from 'react';
import styled from 'styled-components';
import { breakpoints } from '@/styleUtils';
import Divider from '../divider';
import Product from '../product';
import ProfilePicture from '../profilePicture';
import { Post } from '../../types';

const SellerName = styled.p`
  margin: 0px;
  font-size: 0.9rem;
  color: ${(props) => props.theme.colors.fonts.primary.main};
`;

const PostContainer = styled.div`
  margin: 10px 0 20px 0;
  .left-part {
    display: flex;
    align-items: center;
    margin: 6px 0;
  }

  @media only screen and ${breakpoints.sm} {
    display: flex;
    justify-content: center;

    .details {
      width: 100%;
      max-width: 500px;
    }
    hr {
      display: none;
    }
  }
`;

type PostType = {
  product: Post;
};

const PostComponent = ({ product }: PostType) => (
  <PostContainer>
    <div className="details">
      <div className="left-part">
        <ProfilePicture url={product.sellerPicUrl} />
        <SellerName>{product.seller}</SellerName>
      </div>

      <Product details={product} />

      <Divider />
    </div>
  </PostContainer>
);

export default PostComponent;
