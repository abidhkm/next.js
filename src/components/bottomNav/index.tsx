import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { breakpoints } from '@/styleUtils';
import { routes } from '@/constants';

const NavContainer = styled.div`
  position: sticky;
  bottom: 0;
  width: 100%;

  @media only screen and ${breakpoints.sm} {
    display: none;
  }
`;

const BottomNav = () => {
  const router = useRouter();

  return (
    <NavContainer>
      <BottomNavigation
        showLabels
        style={{
          background: `#cecece`,
        }}
      >
        {routes.map(({ href, icon: Icon, label }) => (
          <Link key={href} href={href}>
            <BottomNavigationAction
              label={label}
              icon={
                <Icon
                  style={{
                    fill: router.pathname === href ? `#1976D2` : `#757575`,
                  }}
                />
              }
            />
          </Link>
        ))}
      </BottomNavigation>
    </NavContainer>
  );
};

export default BottomNav;
