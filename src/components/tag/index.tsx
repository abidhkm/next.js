import styled from 'styled-components';

const TagText = styled.span`
  color: ${(props) => props.theme.colors.fonts.primary.main};
  font-size: 0.85rem;
`;

const Tag = ({ text }: { text: string }) => <TagText>#{text} </TagText>;

export default Tag;
