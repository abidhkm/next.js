export type Post = {
  id: number;
  seller: string;
  sellerPicUrl: string;
  imageUrl: string;
  name: string;
  price: number;
  like: number;
  caption: string;
  liked: boolean;
  tags: string[];
  comments: number;
  favorite: boolean;
};
