export const theme = {
  spacing: (value: number) => 6 * value,
  colors: {
    fonts: {
      primary: {
        main: `#1976D2`,
      },
      secondary: {
        main: `#3c3c3c`,
      },
      tertiary: {
        main: `#ffffff`,
      },
    },
    background: {
      primary: {
        main: `#fff`,
        light: `#cecece`,
      },
    },
  },
};

const size = {
  xs: `320px`,
  sm: `768px`,
  lg: `1200px`,
};
const device = {
  xs: `(min-width: ${size.xs})`,
  sm: `(min-width: ${size.sm})`,
  lg: `(min-width: ${size.lg})`,
};

export { device as breakpoints, size };
