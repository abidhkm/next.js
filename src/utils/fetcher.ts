const fetcher = async (url, body) => {
  const res = await fetch(url, { method: `post`, body });
  const data = await res.json();

  if (res.status !== 200) {
    throw new Error(data.message);
  }
  return data;
};

export default fetcher;
