import path from 'path';
import Home from '@material-ui/icons/Home';
import FavoriteIcon from '@material-ui/icons/Favorite';

const postsDirectory = path.join(process.cwd());
export const FILE_PATH = path.join(postsDirectory, `dataFile.json`);

export const routes = [
  {
    href: `/`,
    icon: Home,
    label: `Home`,
  },
  {
    href: `/favorites`,
    icon: FavoriteIcon,
    label: `Favorites`,
  },
];
