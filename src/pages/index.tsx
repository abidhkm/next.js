import { useState } from 'react';
import { Post as PostComponent, Container } from '@/components';
import fs from 'fs';
import fetcher from '@/utils/fetcher';
import { FILE_PATH } from '@/constants';
import { AppContext } from '@/state/productsContext';
import { Post } from '../types';

export default function Home({ posts: _posts }: { posts: Post[] }) {
  const [posts, setPosts] = useState(_posts);

  const handleFavoriteClick = async (body: { [key: string]: string }) => {
    const res = await fetcher(`/api/updateFavorite`, JSON.stringify(body));
    setPosts(res.data);
  };

  return (
    <AppContext.Provider value={{ handleFavoriteClick }}>
      <Container>
        {posts.map((item) => (
          <PostComponent key={item.id} product={item} />
        ))}
      </Container>
    </AppContext.Provider>
  );
}

export async function getStaticProps() {
  const fileContents = fs.readFileSync(FILE_PATH, `utf8`);
  const parsed = JSON.parse(fileContents);

  return {
    props: {
      posts: parsed,
    },
  };
}
