import { AppProps } from 'next/app';
import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { theme } from '@/styleUtils';
import { BottomNav, TopNav } from '@/components';

const GlobalStyle = createGlobalStyle`
html{
  box-sizing: border-box;
  background: #F5F4F0;
  display:block;
  height: 100%;
  margin:0 auto;
  padding: 0;
}

body{
  background-color:#fafafa;
  min-height:100vh;
  margin-top:0;
  font-family:Verdana;
}
`;

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <TopNav />
        <Component {...pageProps} />
        <BottomNav />
      </ThemeProvider>
    </>
  );
}
