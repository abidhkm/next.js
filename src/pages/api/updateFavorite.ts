import { NextApiRequest, NextApiResponse } from 'next';
import path from 'path';
import fs from 'fs';

export default (req: NextApiRequest, res: NextApiResponse) => {
  const { id, value, onlyFavorites = false } = JSON.parse(req.body);

  const postsDirectory = path.join(process.cwd());
  const filePath = path.join(postsDirectory, `dataFile.json`);
  const fileContents = fs.readFileSync(filePath, `utf8`);
  const favorites = JSON.parse(fileContents);
  const index = favorites.findIndex((item) => item.id === id);
  const copy = [...favorites];
  copy[index].favorite = value;

  fs.writeFileSync(filePath, JSON.stringify(copy, null, 4), `utf8`); // TODO: make async

  res.statusCode = 200;
  if (onlyFavorites) {
    res.json({ data: copy.filter((item) => item.favorite) });
  } else {
    res.json({ data: copy });
  }
};
