import { Post as PostComponent, Container } from '@/components';
import fs from 'fs';
import { FILE_PATH } from '@/constants';
import { useState } from 'react';
import fetcher from '@/utils/fetcher';
import { AppContext } from '@/state/productsContext';
import { Post } from '../types';

export default function Home({ posts: _posts }: { posts: Post[] }) {
  const [posts, setPosts] = useState(_posts);

  const handleFavoriteClick = async (body: { [key: string]: string }) => {
    const res = await fetcher(
      `/api/updateFavorite`,
      JSON.stringify({ ...body, onlyFavorites: true }),
    );
    setPosts(res.data);
  };

  return (
    <AppContext.Provider value={{ handleFavoriteClick }}>
      <Container>
        {!posts.length && <p>No favorites!</p>}
        {posts.map((item) => (
          <PostComponent key={item.id} product={item} />
        ))}
      </Container>
    </AppContext.Provider>
  );
}

export async function getStaticProps() {
  const fileContents = fs.readFileSync(FILE_PATH, `utf8`);
  const favorites = JSON.parse(fileContents).filter(
    (product: Post) => product.favorite,
  );

  return {
    props: {
      posts: favorites,
    },
  };
}
